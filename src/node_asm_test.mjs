import * as asm from '../shared/output/asm.mjs';

(async () => {
    const wasm = asm;

    const memory = wasm.memory;

    {
        const summationResult = wasm.add(3, 5);
        console.log('3+5 =', summationResult)
    }

    {
        const encodeString = (string) => {
            // make malloc count divisible by 4
            const memoryNeed = Math.ceil((string.length + 1) / 4) * 4;
            const stringPointer = wasm.wasm_malloc(memoryNeed);
            const stringMemoryView = new Uint8Array(
                memory.buffer, // value
                stringPointer, // offset
                string.length + 1 // length
            );
            for (let i = 0; i < string.length; i++) {
                stringMemoryView[i] = string.charCodeAt(i);
            }
            stringMemoryView[string.length] = 0;
            return stringPointer;
        }

        const decodeString = (memoryView) => {
            let cursor = 0;
            let result = '';

            while (memoryView[cursor] !== 0) {
                result += String.fromCharCode(memoryView[cursor]);
                cursor++;
            }

            return result;
        };

        // pass a string
        const testString = 'Arik says hello!';
        const stringPointer = encodeString(testString);
        const calculatedLength = wasm.calculate_string_length(stringPointer);
        console.log('string length:', calculatedLength)

        // parse a string
        const responseStringPointer = wasm.return_a_string();
        const memoryView = new Uint8Array(memory.buffer, responseStringPointer);
        const responseString = decodeString(memoryView)
        console.log('returned string:', responseString);
    }

    {
		const encodeI32Array = (inputArray) => {
			const cArrayPointer = wasm.wasm_malloc((inputArray.length + 1) * 4);

			const arrayMemoryView = new Uint32Array(memory.buffer, cArrayPointer, inputArray.length + 1);
			arrayMemoryView[0] = inputArray.length;
			arrayMemoryView.set(inputArray, 1);
			return cArrayPointer;
		};

		const decodeI32Array = (arrayPointer, free = true) => {
			const arraySizeViewer = new Uint32Array(
				memory.buffer, // value
				arrayPointer, // offset
				1 // one int
			);
			const arraySize = arraySizeViewer[0];
			const actualArrayViewer = new Int32Array(
				memory.buffer, // value
				arrayPointer, // offset
				arraySize + 1
			);
			const actualArray = actualArrayViewer.slice(1, arraySize + 1);
			if (free) {
				// wasm.free_array(arrayPointer);
				wasm.wasm_free(arrayPointer); // TODO: check if passing *void still captures remaining values
			}
			return actualArray;
		};

        // pass array
        const inputArray = [10, 5, 9, 6, 8, 7];
        const cArrayPointer = encodeI32Array(inputArray);
        const arraySum = wasm.sum_i32_array(cArrayPointer);
        console.log(`array (${JSON.stringify(inputArray)}) sum:`, arraySum)

        // parse array
        const arrayDescriptorPointer = wasm.allocate_i32_array(5);
        const actualArray = decodeI32Array(arrayDescriptorPointer);
        console.log('actual array:', JSON.stringify(Array.from(actualArray)), actualArray);
    }

})();


