import * as fs from 'fs';
const source = fs.readFileSync(`${__dirname}/../shared/output/liblightningjs.wasm`);

(async () => {

    // @ts-ignore
	const memory = new WebAssembly.Memory({initial: 256});



    // @ts-ignore
	const wasmModule = new WebAssembly.Module(source);

    const imports: any = {};
    imports.env = {};

    imports.env.memoryBase = 0;
    imports.env.memory = memory;
    imports.env.tableBase = 0;
    // @ts-ignore
	imports.env.table = new WebAssembly.Table({initial: 4, element: 'anyfunc'});

	imports.env["abort"] = function() {
		console.error("ABORT");
	};
	imports.env["js_free"] = function(argument) {
		console.log('integer passed from wasm:', argument);
	};
	imports.env["js_invoke_function"] = function(fn, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10) {
		console.log('integer passed from wasm:', fn, arg1, arg2);
	};

	imports.env['environ_sizes_get'] = function(){
		console.log('called environ_sizes_get env');
	}

	imports.wasi_snapshot_preview1 = {
		"fd_write" : () => {
			console.log("ABORT");
		},
		"random_get" : () => {
			console.log("ABORT");
		},

		// other
		"environ_sizes_get" : () => {
			console.log("wasi_snapshot_preview1:environ_sizes_get");
		},
		"proc_exit" : () => {
			console.log("wasi_snapshot_preview1:proc_exit");
		},
		"environ_get" : () => {
			console.log("wasi_snapshot_preview1:environ_get");
		},
	};


    // @ts-ignore
	const wasmInstance = await WebAssembly.instantiate(wasmModule, imports)
    const wasm = wasmInstance.exports;


    // register callback
    // const ret = imports.env.table.length;
    // imports.env.table.grow(1);
    // imports.env.table.set(ret, imports.env['custom_callback']);

    // method invocation

    console.log(42);


})();


