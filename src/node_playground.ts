const fs = require('fs');
const source = fs.readFileSync('./invoker.wasm');
const {TextDecoder} = require(String.raw`util`);



(async () => {

    // @ts-ignore
	const memory = new WebAssembly.Memory({initial: 256});

    const heap = new Array(32).fill(undefined);

    heap.push(undefined, null, true, false);

    function getObject(idx) {
        return heap[idx];
    }

    let heap_next = heap.length;

    function dropObject(idx) {
        if (idx < 36) return;
        heap[idx] = heap_next;
        heap_next = idx;
    }

    function takeObject(idx) {
        const ret = getObject(idx);
        dropObject(idx);
        return ret;
    }

    function addHeapObject(obj) {
        if (heap_next === heap.length) heap.push(heap.length + 1);
        const idx = heap_next;
        heap_next = heap[idx];

        heap[idx] = obj;
        return idx;
    }

    let cachegetUint8Memory = null;

    function getUint8Memory() {
        if (cachegetUint8Memory === null || cachegetUint8Memory.buffer !== memory.buffer) {
            cachegetUint8Memory = new Uint8Array(memory.buffer);
        }
        return cachegetUint8Memory;
    }

    let WASM_VECTOR_LEN = 0;

    function passArray8ToWasm(arg) {
        const ptr = wasm.__wbindgen_malloc(arg.length * 1);
        getUint8Memory().set(arg, ptr / 1);
        WASM_VECTOR_LEN = arg.length;
        return ptr;
    }

    let cachedTextDecoder = new TextDecoder('utf-8', {ignoreBOM: true, fatal: true});
    cachedTextDecoder.decode();


    let stack_pointer = 32;

    function addBorrowedObject(obj) {
        if (stack_pointer == 1) throw new Error('out of js stack');
        heap[--stack_pointer] = obj;
        return stack_pointer;
    }


    // @ts-ignore
	const wasmModule = new WebAssembly.Module(source);

    const imports: any = {};
    imports.env = {};

    imports.env.memoryBase = 0;
    imports.env.memory = memory;
    imports.env.tableBase = 0;
    // @ts-ignore
	imports.env.table = new WebAssembly.Table({initial: 4, element: 'anyfunc'});

    imports.env["abort"] = function () {
        console.error("ABORT");
    };

    imports.env["pass_fptr_to_js"] = function (fptr) {
        const method = imports.env.table.get(fptr);
        console.log('this method was called from wasm!');
        // console.log("table index: " + fptr + ", return value: " + imports.env.table.get(fptr)());
    };

    imports.env["pass_integer_to_js"] = function (argument) {
        console.log('integer passed from wasm:', argument);
        // console.log("table index: " + fptr + ", return value: " + imports.env.table.get(fptr)());
    };


    // imports.env['custom_callback']  =  function(someInteger) { console.log('some integer just got called!'); }


    // const wasmHolder = await WebAssembly.instantiate(source, {env: {memory}});
    // const wasmInstance  = wasmHolder.instance;
    // @ts-ignore
	const wasmInstance = await WebAssembly.instantiate(wasmModule, imports)
    const wasm = wasmInstance.exports;


    // register callback
    // const ret = imports.env.table.length;
    // imports.env.table.grow(1);
    // imports.env.table.set(ret, imports.env['custom_callback']);

    // method invocation

    const result = wasm.add(3, 5);
    console.log(result);

    wasm.run_test();

    const nextMultipleOfFour = (number) => {
        return Math.ceil(number / 4) * 4;
    }

    // const lambda = () => { console.log('here'); }
    // const callback_result = wasm.invoke_callback(convertFunction(lambda));


    {
        const encodeString = (string) => {
            // make malloc count divisible by 4
            const memoryNeed = nextMultipleOfFour(string.length + 1);
            const stringPointer = wasm.wasm_malloc(memoryNeed);
            const stringMemoryView = new Uint8Array(
                memory.buffer, // value
                stringPointer, // offset
                string.length + 1 // length
            );
            for (let i = 0; i < string.length; i++) {
                stringMemoryView[i] = string.charCodeAt(i);
            }
            stringMemoryView[string.length] = 0;
            return stringPointer;
        }

        const decodeString = (stringPointer, free = true) => {
            const memoryView = new Uint8Array(memory.buffer, stringPointer);
            let cursor = 0;
            let result = '';

            while (memoryView[cursor] !== 0) {
                result += String.fromCharCode(memoryView[cursor]);
                cursor++;
            }

            if (free) {
                wasm.wasm_free(stringPointer);
            }

            return result;
        };

        // pass a string
        const testString = 'Arik says hello!';
        const stringPointer = encodeString(testString);
        const calculatedLength = wasm.calculate_string_length(stringPointer);
        console.log('string length:', calculatedLength)

        // parse a string
        const responseStringPointer = wasm.return_a_string();
        const responseString = decodeString(responseStringPointer);

        const secondStringPointer = wasm.return_a_string();
        const secondString = decodeString(secondStringPointer, false);

        const thirdStringPointer = wasm.return_a_string();
        console.log('returned string:', responseString);
    }


    {
        const encodeArray = (inputArray) => {
            // TODO: (matt) is this correct, or should it go back to length * 4?
            // const cArrayPointer = wasm.wasm_malloc(inputArray.length * 4);
            const cArrayPointer = wasm.wasm_malloc(nextMultipleOfFour(inputArray.length));

            const arrayMemoryView = new Uint32Array(memory.buffer, cArrayPointer, inputArray.length);
            arrayMemoryView.set(inputArray);
            return cArrayPointer;
        }

        const decodeArray = (arrayPointer, free = true) => {
            const arraySizeViewer = new Uint32Array(
                memory.buffer, // value
                arrayPointer, // offset
                1 // one int
            );
            const arraySize = arraySizeViewer[0];
            const actualArrayViewer = new Uint32Array(
                memory.buffer, // value
                arrayPointer, // offset
                arraySize + 1
            );
            const actualArray = actualArrayViewer.slice(1, arraySize + 1);
            if (free) {
                // wasm.free_array(arrayPointer);
                wasm.wasm_free(arrayPointer); // TODO: check if passing *void still captures remaining values
            }
            return actualArray;
        }

        // pass array
        const inputArray = [10, 5, 9, 6, 8, 7];
        const cArrayPointer = encodeArray(inputArray);
        const arraySum = wasm.sum(cArrayPointer, inputArray.length * 4);
        console.log(`array (${JSON.stringify(inputArray)}) sum:`, arraySum)


        // parse array
        const firstPointer = wasm.allocate_array(7, 100);
        const firstArray = decodeArray(firstPointer, true);

        const reDecodedArray = decodeArray(firstPointer, false);

        const secondPointer = wasm.allocate_array(13, 200);
        const secondArray = decodeArray(secondPointer, false);

        const thirdPointer = wasm.allocate_array();

        console.log('pointer, free, pointer, pointer:', firstPointer, secondPointer, thirdPointer);
        console.log('returned array:', JSON.stringify(Array.from(secondArray)), secondArray);
    }


})();


