#!/bin/bash

clang --target=wasm32-unknown-wasi --sysroot /wasi-libc -nostartfiles -I./ -Wl,--import-memory -Wl,--no-entry -Wl,--export-all -Wl,-allow-undefined -s -Os -o /app/output/ldk.wasm /app/input/bindings.c
