#!/bin/bash

COMMON_COMPILE="clang -std=c11 -Wall -Wno-nullability-completeness -Wextra -Wno-unused-parameter -Wno-ignored-qualifiers -Wno-unused-function -Wno-pointer-sign"

set -e
set -x

DEBUG="false"
LOCATION_PREFIX=`pwd`


COMPILE="$COMMON_COMPILE -flto -Wl,--no-entry -Wl,--export-dynamic -nostdlib --target=wasm32-unknown-unknown -o liblightningjs.wasm"
# We only need malloc and assert/abort, but for now just use WASI for those:
EXTRA_LINK=/usr/lib/wasm32-wasi/libc.a
if [ $DEBUG = "true" ]; then
	$COMPILE -g -Wl,-wrap,calloc -Wl,-wrap,realloc -Wl,-wrap,reallocarray -Wl,-wrap,malloc -Wl,-wrap,free -I"$LOCATION_PREFIX"/lightning-c-bindings/include/ ts/bindings.c "$LOCATION_PREFIX"/lightning-c-bindings/target/wasm32-unknown-unknown/debug/libldk.a $EXTRA_LINK
else
	$COMPILE -s -Os -I"$LOCATION_PREFIX"/ invoker.c "$LOCATION_PREFIX"/libldk.a $EXTRA_LINK
fi
