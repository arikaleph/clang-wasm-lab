#!/bin/bash

# clang -std=c11 -Wall -Wno-nullability-completeness -Wextra -Wno-unused-parameter -Wno-ignored-qualifiers -Wno-unused-function -Wno-pointer-sign -flto -Wl,--no-entry -Wl,--export-dynamic -nostdlib --target=wasm32-unknown-unknown -o liblightningjs.wasm -s -Os -I/Users/arik/Developer/ldk-java/lab/bindings/typescript/invocation/ invoker.c /Users/arik/Developer/ldk-java/lab/bindings/typescript/invocation/libldk.a /usr/lib/wasm32-wasi/libc.a
# clang -std=c11 -Wall -Wno-nullability-completeness -Wextra -Wno-unused-parameter -Wno-ignored-qualifiers -Wno-unused-function -Wno-pointer-sign -flto -Wl,--no-entry -Wl,--export-dynamic -nostdlib --target=wasm32-unknown-unknown -o liblightningjs.wasm -s -Os -I/Users/arik/Developer/ldk-java/lab/bindings/typescript/invocation/ invoker.c
# clang invoker.c -o run_invoker


# export CC=clang-9;
# clang -std=c11 invoker.c --target=wasm32-unknown-unknown -o invoker.wasm /usr/lib/wasm32-wasi/libc.a


# clang --target=wasm32 --no-standard-libraries -Wl,--export-all -Wl,--no-entry -o invoker.wasm invoker.c
clang --target=wasm32-unknown-wasi --sysroot /wasi-libc -nostartfiles -I./ -Wl,--import-memory -Wl,--no-entry -Wl,--export-all -Wl,-allow-undefined -s -Os -o /app/output/invoker.wasm /app/input/invoker.c
#clang --target=wasm32-unknown-wasi \
#      --sysroot /tmp/wasi-libc \
#      -nostartfiles \
#      -Wl,--import-memory \
#      -Wl,--no-entry \
#      -Wl,--export-all \
#      -o invoker.wasm \
#      invoker.c

