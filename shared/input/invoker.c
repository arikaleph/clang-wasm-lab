//#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//#include "web_console.h"
//#include "js-wasm.h"

int add(int first, int second) {
//  printf("Hello, World! \n");
    return first + second;
}

typedef void(*lambda_type)(void);

void invoke_callback(lambda_type lambda) {
    lambda();
}

int copy(char *input, char *output) {
    const int length = strlen(input);

    strncpy(output, input, length);

    return length;
}


char *malloc_copy(char *input) {
    char *result = malloc(1024);

    strncpy(result, input, strlen(input));

    return result;
}


// CALLBACK STUFF START

extern void pass_integer_to_js(int number);

void trigger_integer_callback(int value_to_return) {
    pass_integer_to_js(value_to_return);
}

// CALLBACK STUFF END


// ARRAY STUFF START

uint8_t sum_u8_array(uint8_t a[]) {
    int sum = 0;
    int length = a[0];
    for (unsigned int i = 1; i <= length; i++) {
        sum += a[i];
    }
    return sum;
}

uint8_t *allocate_u8_array(unsigned int arraySize) {
    uint8_t *array = malloc((arraySize + 4) * sizeof(uint8_t));
    if (!array) {
        return NULL;
    }

    // split the u32 length value into 4 u8s
    array[0] = (arraySize >> 24) & 0xFF;
    array[1] = (arraySize >> 16) & 0xFF;
    array[2] = (arraySize >> 8) & 0xFF;
    array[3] = arraySize & 0xFF;

    // fill with index values -8
    for (unsigned int i = 4; i < arraySize + 4; ++i) {
        array[i] = i + 4; // +8 for u8 (and -4 for index offset)
    }

    return array;
}

signed int sum_i32_array(signed int a[]) {
    int sum = 0;
    int length = a[0];
    for (unsigned int i = 1; i <= length; i++) {
        sum += a[i];
    }
    return sum;
}

signed int *allocate_i32_array(unsigned int arraySize) {
    signed int *array = malloc((arraySize + 1) * sizeof(int));
    if (!array) {
        return NULL;
    }

    array[0] = arraySize; // first byte to indicate array length

    // fill with index + 10
    for (unsigned int i = 1; i <= arraySize; ++i) {
        array[i] = i - 33; // -32 for i32 (and -1 for index offset)
    }

    return array;
}

// ARRAY STUFF END


int calculate_string_length(const char *someString) {
    int stringLength = strlen(someString);
    return stringLength;
}

char *return_a_string() {
    char *str = malloc(24 + 1);

    str[24] = 0;

    str[0] = 'H';
    str[1] = 'e';
    str[2] = 'l';
    str[3] = 'l';
    str[4] = 'o';
    str[5] = ' ';
    str[6] = 'f';
    str[7] = 'r';
    str[8] = 'o';
    str[9] = 'm';
    str[10] = ' ';
    str[11] = 'C';
    str[12] = ',';
    str[13] = ' ';
    str[14] = 's';
    str[15] = 'a';
    str[16] = 'y';
    str[17] = 's';
    str[18] = ' ';
    str[19] = 'A';
    str[20] = 'r';
    str[21] = 'i';
    str[22] = 'k';
    str[23] = '!';
//    str = "Hello from C, says Arik!"; // change the value
    return str;
}

const char *get_substring(const char *string, int startIndex, int length) {
    char subbuff[length + 1];
    memcpy(subbuff, &string[startIndex], length);
    subbuff[length] = '\0';
    return subbuff;
}


// WASM MEMORY MANAGEMENT START

extern unsigned char __heap_base;

unsigned int bump_pointer = &__heap_base;

void *wasm_malloc(int n) {
    unsigned int r = bump_pointer;
    bump_pointer += n;
    return (void *) r;
}

void wasm_free(void *p) {
    free(p);
}

// WASM MEMORY MANAGEMENT END



// EXPERIMENTAL STUFF

/*


typedef int (*fptr_type)(void);

typedef void (*custom_callback)(int);

static int callback_0(void) {
    return 26;
}

static int callback_1(void) {
    return 42;
}

void run_test()
{
    pass_fptr_to_js(callback_0);
    pass_fptr_to_js(callback_1);
    pass_integer_to_js(38);
}
*/


/*
export int print_to_console() {
	console_log("hello");
	console_log("world");
	return 0;
}
*/


//int * create_array() {
//    static int array[5];
//    array[0] = 23;
//    array[1] = 19;
//    array[2] = 1000;
//    array[3] = 0;
//    array[4] = 7700;
//
//    static int arrayPointer[2];
//    arrayPointer[0] = 5;
//    arrayPointer[1] = array;
//
//    return arrayPointer;
//}
