import * as fs from 'fs';

const asmInputPath = `${__dirname}/../shared/output/asm_raw.js`;
const asmVanillaOutputPath = `${__dirname}/../shared/output/asm.js`;
const asmModularOutputPath = `${__dirname}/../shared/output/asm.mjs`;

const asmInput = fs.readFileSync(asmInputPath).toString();

{
	// VANILLA REPLACEMENTS
	let asmVanillaOutput = asmInput;

	const importPattern = new RegExp(`import (.*) from 'env'`, 'gm');
	asmVanillaOutput = asmVanillaOutput.replace(importPattern, `const $1 = require('../../src/env.js')`);

	const memInitPattern = new RegExp('(var memasmFunc = .*;)\\s*(var retasmFunc)');
	asmVanillaOutput = asmVanillaOutput.replace(memInitPattern, `$1\nconst memory = { buffer : memasmFunc };\nconst moduleExports = { memory };\n$2`);

	asmVanillaOutput = asmVanillaOutput.replace('memory: { buffer : memasmFunc }', 'memory');

	const exportPattern = new RegExp('export var\\s*',  'g');
	asmVanillaOutput = asmVanillaOutput.replace(exportPattern, 'moduleExports.');

	asmVanillaOutput += 'module.exports = moduleExports;\n';

	fs.writeFileSync(asmVanillaOutputPath, asmVanillaOutput);
}

{
	// MODULAR (.mjs) REPLACEMENTS
	let asmModularOutput = asmInput;

	const importPattern = new RegExp(`(import.*from) 'env'`, 'gm');
	asmModularOutput = asmModularOutput.replace(importPattern, `$1 '../../src/env.mjs'`);

	const memInitPattern = new RegExp('(var memasmFunc = .*;)\\s*(var retasmFunc)');
	asmModularOutput = asmModularOutput.replace(memInitPattern, `$1\nexport var memory = { buffer : memasmFunc };\n$2`);

	asmModularOutput = asmModularOutput.replace('memory: { buffer : memasmFunc }', 'memory');

	fs.writeFileSync(asmModularOutputPath, asmModularOutput);
}
