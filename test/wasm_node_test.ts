import chai = require('chai');
import * as fs from 'fs';

const assert = chai.assert;

const nextMultipleOfFour = (number) => {
	return Math.ceil(number / 4) * 4;
};

describe('WASM Node Test', () => {

	// @ts-ignore
	const memory = new WebAssembly.Memory({initial: 256});
	let wasm;

	let latestIntegerCallbackValue = null;

	before(async () => {
		const source = fs.readFileSync(`${__dirname}/../shared/output/invoker.wasm`);
		// @ts-ignore
		const wasmModule = new WebAssembly.Module(source);

		const imports: any = {};
		imports.env = {};

		imports.env.memoryBase = 0;
		imports.env.memory = memory;
		imports.env.tableBase = 0;
		// @ts-ignore
		imports.env.table = new WebAssembly.Table({initial: 4, element: 'anyfunc'});

		imports.env['abort'] = function () {
			console.error('ABORT');
		};

		imports.env['pass_integer_to_js'] = function (argument) {
			latestIntegerCallbackValue = argument;
		};


		// @ts-ignore
		const wasmInstance = await WebAssembly.instantiate(wasmModule, imports);
		wasm = wasmInstance.exports;
	});

	it('should test summation', () => {
		let sum = wasm.add(1, 2);
		assert.equal(sum, 3);

		const a = Math.round(Math.random() * 400);
		const b = Math.round(Math.random() * 400);
		sum = wasm.add(a, b);
		assert.equal(sum, a + b);
	});

	describe('String Codec Tests', () => {

		const encodeString = (string: string): number => {
			// make malloc count divisible by 4
			const memoryNeed = nextMultipleOfFour(string.length + 1);
			const stringPointer = wasm.wasm_malloc(memoryNeed);
			const stringMemoryView = new Uint8Array(
				memory.buffer, // value
				stringPointer, // offset
				string.length + 1 // length
			);
			for (let i = 0; i < string.length; i++) {
				stringMemoryView[i] = string.charCodeAt(i);
			}
			stringMemoryView[string.length] = 0;
			return stringPointer;
		};

		const decodeString = (stringPointer: number, free: boolean = true): string => {
			const memoryView = new Uint8Array(memory.buffer, stringPointer);
			let cursor = 0;
			let result = '';

			while (memoryView[cursor] !== 0) {
				result += String.fromCharCode(memoryView[cursor]);
				cursor++;
			}

			if (free) {
				wasm.wasm_free(stringPointer);
			}

			return result;
		};

		it('should pass string to wasm', () => {
			const testString = 'Arik says hello!';
			const stringPointer = encodeString(testString);
			const calculatedLength = wasm.calculate_string_length(stringPointer);
			assert.equal(calculatedLength, testString.length);
		});

		it('should receive string wasm', () => {
			const stringPointer = wasm.return_a_string();
			const string = decodeString(stringPointer);
			assert.equal(string, 'Hello from C, says Arik!');
		});

	});

	describe('Array Codec Tests', () => {

		const encodeU8Array = (inputArray: number[]): number => {
			const cArrayPointer = wasm.wasm_malloc(nextMultipleOfFour(inputArray.length + 1));

			const arrayMemoryView = new Uint8Array(memory.buffer, cArrayPointer, inputArray.length + 1);
			arrayMemoryView[0] = inputArray.length;
			arrayMemoryView.set(inputArray, 1);
			return cArrayPointer;
		};

		const encodeI32Array = (inputArray: number[]): number => {
			const cArrayPointer = wasm.wasm_malloc((inputArray.length + 1) * 4);

			const arrayMemoryView = new Uint32Array(memory.buffer, cArrayPointer, inputArray.length + 1);
			arrayMemoryView[0] = inputArray.length;
			arrayMemoryView.set(inputArray, 1);
			return cArrayPointer;
		};

		const decodeU8Array = (arrayPointer: number, free: boolean = true): Uint8Array => {
			const arraySizeViewer = new Uint8Array(
				memory.buffer, // value
				arrayPointer, // offset
				4 // one int
			);

			const arraySizeBuffer = Buffer.from(arraySizeViewer);
			const arraySize = arraySizeBuffer.readUInt32BE(0);
			const actualArrayViewer = new Uint8Array(
				memory.buffer, // value
				arrayPointer, // offset
				arraySize + 4
			);
			const actualArray = actualArrayViewer.slice(4, arraySize + 4);
			if (free) {
				// wasm.free_array(arrayPointer);
				wasm.wasm_free(arrayPointer); // TODO: check if passing *void still captures remaining values
			}
			return actualArray;
		};

		const decodeI32Array = (arrayPointer: number, free: boolean = true): Int32Array => {
			const arraySizeViewer = new Uint32Array(
				memory.buffer, // value
				arrayPointer, // offset
				1 // one int
			);
			const arraySize = arraySizeViewer[0];
			const actualArrayViewer = new Int32Array(
				memory.buffer, // value
				arrayPointer, // offset
				arraySize + 1
			);
			const actualArray = actualArrayViewer.slice(1, arraySize + 1);
			if (free) {
				// wasm.free_array(arrayPointer);
				wasm.wasm_free(arrayPointer); // TODO: check if passing *void still captures remaining values
			}
			return actualArray;
		};

		describe('Array u8 Encoding Tests', () => {
			it('should test array encoding', () => {
				const inputArray = [10, 5, 9, 6, 8, 7];
				const cArrayPointer = encodeU8Array(inputArray);
				const sum = wasm.sum_u8_array(cArrayPointer);

				const expectedSum = inputArray.reduce((a, b) => a + b, 0);
				assert.equal(sum, expectedSum);
			});

			it('should test summing numbers outside u8 range', () => {
				const inputArray = [301, 500, 27942, 69420, 42069]; // tribute to Elon
				const cArrayPointer = encodeU8Array(inputArray);
				const sum = wasm.sum_u8_array(cArrayPointer);

				const expectedSum = inputArray.reduce((a, b) => a + b, 0) % 256;
				assert.equal(sum, expectedSum);
			});

			it('should sum negative numbers', () => {
				const inputArray = [-5, -900, 300, -70]; // tribute to Elon
				const cArrayPointer = encodeU8Array(inputArray);
				const sum = wasm.sum_u8_array(cArrayPointer);

				// TODO: it works, but should it…?
				let expectedSum = inputArray.reduce((a, b) => a + b, 0);
				expectedSum = ((expectedSum % 256) + 256) % 256;
				assert.equal(sum, expectedSum);
			});
		});

		describe('Array i32 Encoding Tests', () => {
			it('should test array encoding', () => {
				const inputArray = [10, 5, 9, 6, 8, 7];
				const cArrayPointer = encodeI32Array(inputArray);
				const sum = wasm.sum_i32_array(cArrayPointer);

				const expectedSum = inputArray.reduce((a, b) => a + b, 0);
				assert.equal(sum, expectedSum);
			});

			it('should test summing numbers outside u8 range', () => {
				const inputArray = [301, 500, 27942, 69420, 42069]; // tribute to Elon
				const cArrayPointer = encodeI32Array(inputArray);
				const sum = wasm.sum_i32_array(cArrayPointer);

				const expectedSum = inputArray.reduce((a, b) => a + b, 0);
				assert.equal(sum, expectedSum);
			});

			it('should sum negative numbers', () => {
				const inputArray = [-5, -900, 300, -70]; // tribute to Elon
				const cArrayPointer = encodeI32Array(inputArray);
				const sum = wasm.sum_i32_array(cArrayPointer);

				// TODO: it works, but should it…?
				const expectedSum = inputArray.reduce((a, b) => a + b, 0);
				assert.equal(sum, expectedSum);
			});
		});

		describe('Array u8 Decoding Tests', () => {

			it('should decode a fixed array', () => {
				const arrayPointer = wasm.allocate_u8_array(7);
				const array = decodeU8Array(arrayPointer, true);

				assert.equal(array.length, 7);
				assert.equal(array[0], 8);
				assert.equal(array[1], 9);
				assert.equal(array[2], 10);
				assert.equal(array[3], 11);
				assert.equal(array[4], 12);
				assert.equal(array[5], 13);
				assert.equal(array[6], 14);
			});

			it('should decode an array longer than u8', () => {
				const arrayPointer = wasm.allocate_u8_array(3715);
				const array = decodeU8Array(arrayPointer, true);

				assert.equal(array.length, 3715);
				assert.equal(array[0], 8);
				assert.equal(array[1], 9);
				assert.equal(array[2], 10);
				assert.equal(array[3713], (3713 + 8) % 256);
				assert.equal(array[3714], (3714 + 8) % 256);
			});

			it('should verify deallocation allows memory reuse', () => {
				const firstPointer = wasm.allocate_u8_array(7);
				const firstArray = decodeU8Array(firstPointer, true); // freed

				const secondPointer = wasm.allocate_u8_array(13);
				const secondArray = decodeU8Array(secondPointer, false); // not freed

				const thirdPointer = wasm.allocate_u8_array();

				assert.equal(firstPointer, secondPointer); // the freeing should have allowed for the same memory use
				assert.notEqual(firstPointer, thirdPointer); // the freeing should have allowed for the same memory use
			});
		});

		describe('Array i32 Decoding Tests', () => {

			it('should decode a fixed array', () => {
				const arrayPointer = wasm.allocate_i32_array(7);
				const array = decodeI32Array(arrayPointer, true);

				assert.equal(array.length, 7);
				assert.equal(array[0], -32);
				assert.equal(array[1], -31);
				assert.equal(array[2], -30);
				assert.equal(array[3], -29);
				assert.equal(array[4], -28);
				assert.equal(array[5], -27);
				assert.equal(array[6], -26);
			});

			it('should decode an array longer than u8', () => {
				const arrayPointer = wasm.allocate_i32_array(3715);
				const array = decodeI32Array(arrayPointer, true);

				assert.equal(array.length, 3715);
				assert.equal(array[0], -32);
				assert.equal(array[1], -31);
				assert.equal(array[2], -30);
				assert.equal(array[3713], 3713 - 32);
				assert.equal(array[3714], 3714 - 32);
			});

			it('should verify deallocation allows memory reuse', () => {
				const firstPointer = wasm.allocate_i32_array(7);
				const firstArray = decodeI32Array(firstPointer, true); // freed

				const secondPointer = wasm.allocate_i32_array(13);
				const secondArray = decodeI32Array(secondPointer, false); // not freed

				const thirdPointer = wasm.allocate_i32_array();

				assert.equal(firstPointer, secondPointer); // the freeing should have allowed for the same memory use
				assert.notEqual(firstPointer, thirdPointer); // the freeing should have allowed for the same memory use
			});
		});

		describe('Callback Tests', () => {
			it('should verify callbacks get called', () => {
				wasm.trigger_integer_callback(170);
				assert.equal(latestIntegerCallbackValue, 170);
			});
		});

	});

});
