# Clang WASM Lab

## Setup

1. Build Docker image

```shell
host $ docker build --tag wasm-compiler ./
```

2. Launch Docker shell

```shell
host $ ./bin/docker.sh
```

## Iterate

Apply C changes to `invoker.c`.

Whenever you want to compile them, 
inside the Docker shell, run:

```shell
docker $ ./compile_direct.sh
```

Apply JavaScript changes to `node_adder.js` for NodeJS and to `adder.html` for Web.

For NodeJS changes, on your host machine, simply run 

```shell
host $ node ./node_adder.js
```

## Test

(If you haven't already, make sure you have npm installed and that you have run `npm install` in the project directory.)

On your host, run the following command:

```shell
host $ npm run tsc && npm test
```

## ASM

To generate the ASM file, in the Docker shell, run:

```shell
docker $ ./asmify.sh
```

That's not enough, however. A couple modifications need to be applied to the file. To fix asm.mjs, run the following
in your host shell:

```shell
host $ node ./bin/convert.asm.js
```

The script should be idempotent, so there ought not be any problems caused by running it multiple times.

Now you can run the ASM version in Node thus:

```shell
host $ node --experimental-modules ./node_asm_test.mjs
```

These modifications will soon be automated.

