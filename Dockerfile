# 1. Base OS Image
FROM debian:testing

RUN apt-get -y update
RUN apt-get install -y bash llvm lld clang git make cmake

RUN git clone https://github.com/WebAssembly/wasi-libc.git /wasi-libc && make -C /wasi-libc install INSTALL_DIR=/wasi-libc

COPY ./shared/bin/libclang_rt.builtins-wasm32.a /usr/lib/llvm-11/lib/clang/11.0.1/lib/wasi/libclang_rt.builtins-wasm32.a

RUN git clone https://github.com/WebAssembly/binaryen.git /binaryen && cd /binaryen && cmake . && make

RUN mkdir -p /app/bin
WORKDIR /app/bin

ENV SHELL=/bin/bash

# RUN clang --target=wasm32 --no-standard-libraries -Wl,--export-all -Wl,--no-entry -o invoker.wasm invoker.c

# CMD ["/app/src/bitcoind", "-conf=/app/config/bitcoin.conf"]

# /app/src/bitcoind -conf=/app/config/bitcoin.conf
